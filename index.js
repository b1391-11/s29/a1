

const express = require("express");
const app = express();
const PORT = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

/*PART 1*/

app.get("/home", (req, res) => res.send(`Welcome to my Home Page`));


/*PART 2*/

let users = [
	{
		username: "Moy",
		password: "moy12345"
	}
];

app.get("/users", (req, res) => {
	res.send(users);
})


/*PART 3*/

app.delete("/delete-users", (req, res) => {
	users.push(req.body.username);
	res.send(`User ${req.body.username} has been deleted.`)
})

app.listen(PORT, () => console.log(`Server is running at port ${PORT}`));
